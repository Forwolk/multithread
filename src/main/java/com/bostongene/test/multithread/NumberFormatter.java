package com.bostongene.test.multithread;

public class NumberFormatter {

    public static int format (String string) {
        int number = 0;
        String[] parts = string.split(" ");
        for (int i = 0; i < parts.length; i++) {
            if (i != parts.length - 1) {
                int m = getFactor(parts[i + 1]);
                if (m != -1)
                    number += getNumber(parts[i]) * m;
                else
                    number += getNumber(parts[i]);
                continue;
            }
            number += getNumber(parts[i]);
        }
        return number;
    }

    private static int getFactor (String word) {
        switch (word.toLowerCase()) {
            case "hundred":
                return 100;
            case "thousand":
                return 1000;
            default:
                return -1;
        }
    }

    private static int getNumber (String num) {
        switch (num.toLowerCase()) {
            case "one":
                return 1;
            case "two":
                return 2;
            case "three":
                return 3;
            case "four":
                return 4;
            case "five":
                return 5;
            case "six":
                return 6;
            case "seven":
                return 7;
            case "eight":
                return 8;
            case "nine":
                return 9;
            case "zero":
                return 0;
            case "ten":
                return 10;
            case "twenty":
                return 20;
            case "thirty":
                return 30;
            case "forty":
                return 40;
            case "fifty":
                return 50;
            case "sixty":
                return 60;
            case "seventy":
                return 70;
            case "eighty":
                return 80;
            case "ninety":
                return 90;
            default:
                return 0;
        }
    }
}
