package com.bostongene.test.multithread;

import java.util.Scanner;

public class Main {

    private final static DAO dao = new InMemoryDAO();
    private static boolean working = true;

    public static void main (String... args) {

        new Thread(() -> {
            while (working) {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (dao) {
                    int num = dao.getMinimum();
                    if (num != -1)
                        System.out.println(num);
                }
            }
        }).start();

        new Thread(() -> {
            Scanner in = new Scanner(System.in);
            while (working) {
                String string = in.nextLine();
                synchronized (dao) {
                    dao.addNumber(string);
                }
            }
        }).start();
    }
}
