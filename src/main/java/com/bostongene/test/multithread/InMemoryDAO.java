package com.bostongene.test.multithread;

import java.util.HashSet;
import java.util.Set;

public class InMemoryDAO implements DAO {

    private Set<Integer> numbers = new HashSet<>();

    /**
     * Записать число в виде числительных на английском языке
     *
     * @param number числительное на английском языке
     */
    @Override
    public void addNumber(String number) {
        numbers.add(NumberFormatter.format(number));
    }

    /**
     * Получить минимальное записанное число
     *
     * @return минимальное число
     */
    @Override
    public int getMinimum() throws RuntimeException{
        int min = -1;
        for (int n : numbers) {
            if (min == -1)
                min = n;
            if (min > n)
                min = n;
        }
        return min;
    }
}
