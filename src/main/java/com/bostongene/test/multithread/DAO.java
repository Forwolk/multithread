package com.bostongene.test.multithread;

public interface DAO {

    /**
     * Записать число в виде числительных на английском языке
     *
     * @param number числительное на английском языке
     */
    void addNumber (String number);

    /**
     * Получить минимальное записанное число
     *
     * @return минимальное число
     */
    int getMinimum ();
}
