package com.bostongene.test.multithread;

import org.junit.Test;

import static org.junit.Assert.*;

public class NumberFormatterTest {

    @Test
    public void formattingTestHundred () {
        int testNumber1 = NumberFormatter.format("one hundred");
        int testNumber2 = NumberFormatter.format("sixty six");
        int testNumber3 = NumberFormatter.format("four");

        assertEquals(testNumber1, 100);
        assertEquals(testNumber2, 66);
        assertEquals(testNumber3, 4);
    }

    @Test
    public void formattingTestThousand (){
        int testNumber1 = NumberFormatter.format("three thousand five");
        int testNumber2 = NumberFormatter.format("six thousand four hundred twenty three");
        int testNumber3 = NumberFormatter.format("nine thousand nine hundred ninety nine");

        assertEquals(testNumber1, 3005);
        assertEquals(testNumber2, 6423);
        assertEquals(testNumber3, 9999);
    }
}
